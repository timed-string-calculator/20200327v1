﻿using NUnit.Framework;
using StringCalculator1;
using System;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestAdd
    {
        Calculator _calculator = null;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("1,2");

            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GIVEN_MultipleNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("1,2,3");

            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GIVEN_NewLineAsDelimietr_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("1,2\n3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("//;\n1;2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumbers_WHEN_Adding_THEN_ThrowException()
        {
            var expected = "Negative numbers not allowed -5,-6";
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;2;-5;-6"));

            Assert.AreEqual(expected,actual.Message);
        }
    }
}
