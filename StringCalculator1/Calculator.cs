﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculator1
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string[] delimiters = GetDelimiters(numbers);
            List<int> numbersArray = GetNumbers(numbers, delimiters);
            ValidateNumbers(numbersArray);

            return GetSum(numbersArray);
        }

        private void ValidateNumbers(List<int> numbersArray)
        {
            List<string> negativeNumbers = new List<string>();

            foreach (var number in numbersArray)
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number.ToString());
                }
            }

            if (negativeNumbers.Count != 0)
            {
                throw new Exception($"Negative numbers not allowed {string.Join("," , negativeNumbers)}");
            }
        }

        public string[] GetDelimiters(string numbers)
        {
            var customDelimiterId = "//";
            var newline = "\n";

            if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", "\n" };
        }

        public List<int> GetNumbers(string numbers, string[] delimiters)
        {
            List<int> numbersArray = new List<int>();
            string[] stringNumbers = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (var num in stringNumbers)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersArray.Add(number);
                }
            }

            return numbersArray;
        }

        private int GetSum(List<int> numbersArray)
        {
            int sum = 0;

            foreach (var number in numbersArray)
            {
                sum += number;
            }

            return sum;
        }
    }
}
